package com.fyan.dev.daycare;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.fyan.dev.daycare.api.APIConfig;
import com.fyan.dev.daycare.models.Token;
import com.fyan.dev.daycare.models.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {

    EditText edt_username,edit_password;
    Button btn_login;
    ProgressDialog progressDialog;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    //comment tambahan

    private void cobaNgepush(){
        Toast.makeText(Login.this,
                "Barusan coba ngepush", Toast.LENGTH_SHORT).show();
    }

    
    //comment hasan
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

//        /ini apa ya ???. ADL
        edt_username = findViewById(R.id.edt_username);
        edit_password = findViewById(R.id.edit_password);
        btn_login = findViewById(R.id.btn_login);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Login();
            }
        });

    }

    //coba SDW
    private void Login(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Login to server");
        progressDialog.setCancelable(false);
        progressDialog.show();

        User user = new User();
        user.setUsername(edt_username.getText().toString());
        user.setPassword(edit_password.getText().toString());

        Call<Token> call = APIConfig.getRetrofit().login(user);
        call.enqueue(new Callback<Token>() {
            @Override
            public void onResponse(Call<Token> call, Response<Token> response) {
                progressDialog.dismiss();
                if (response.body() != null) {
                    String token = response.body().getToken();
                    String expired_date = response.body().getExpired_date();
                    int expired = response.body().getExpired();
                    boolean success = response.body().isSuccess();
                    if (success) {
                        preferences = getApplicationContext().
                                getSharedPreferences("training", MODE_PRIVATE);
                        editor = preferences.edit();
                        editor.putString("token", token);
                        editor.putBoolean("isLogin", success);
                        editor.apply();

                        Intent intent = new Intent(Login.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else {
                        Toast.makeText(Login.this,
                                "Login gagal", Toast.LENGTH_SHORT).show();

                    }
                    //testing rama
                }
                else {
                    Toast.makeText(Login.this,
                            "Login gagal", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Token> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(Login.this,
                        "Login gagal", Toast.LENGTH_SHORT).show();

            }
        });
    }
}
