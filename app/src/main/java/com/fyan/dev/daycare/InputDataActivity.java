package com.fyan.dev.daycare;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.fyan.dev.daycare.api.APIConfig;
import com.fyan.dev.daycare.models.Employee;
import com.fyan.dev.daycare.models.ResponseValue;
import com.orm.query.Select;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InputDataActivity extends AppCompatActivity {
    EditText txtNrp, txtNama, txtAlamat;
    Button btnSimpan;
    boolean modeEdit;
    ProgressDialog progressDialog;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_data);

        preferences = getApplicationContext().
                getSharedPreferences("training",MODE_PRIVATE);
        modeEdit = getIntent().getBooleanExtra("modeEdit",false);

        String nrp = getIntent().getStringExtra("nrp");
        String nama = getIntent().getStringExtra("nama");
        String alamat = getIntent().getStringExtra("alamat");

        txtNrp = (EditText) findViewById(R.id.txtNrp);
        txtNama = (EditText) findViewById(R.id.txtNama);
        txtAlamat = (EditText) findViewById(R.id.txtAlamat);
        btnSimpan = (Button) findViewById(R.id.btnSave);

        if (modeEdit){
           txtNrp.setText(nrp);
           txtNama.setText(nama);
           txtAlamat.setText(alamat);
           txtNrp.setEnabled(false);
        }

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (modeEdit){
//                    btnEditClick();
                    editToAPI();
                }
                else {
//                    btnSimpanClick();
                    saveToAPI();
                }
            }
        });
    }

    private void saveToAPI(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Save to server");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Employee employee = new Employee();
        employee.setNrp(txtNrp.getText().toString());
        employee.setNama(txtNama.getText().toString());
        employee.setAlamat(txtAlamat.getText().toString());

        Call<ResponseValue> call = APIConfig.getRetrofit().
                insertEmployee(preferences.getString("token",""),employee);
        call.enqueue(new Callback<ResponseValue>() {
            @Override
            public void onResponse(Call<ResponseValue> call, Response<ResponseValue> response) {
                progressDialog.dismiss();
                if (response.body() != null){
                    boolean status = response.body().isStatus();
                    String remarks = response.body().getRemarks();
                    if (status){
                        Toast.makeText(InputDataActivity.this,
                                remarks, Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else {
                        Toast.makeText(InputDataActivity.this,
                                remarks, Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(InputDataActivity.this,
                            "Save Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseValue> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(InputDataActivity.this,
                        "Save Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void editToAPI(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Save to server");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Employee employee = new Employee();
        employee.setNrp(txtNrp.getText().toString());
        employee.setNama(txtNama.getText().toString());
        employee.setAlamat(txtAlamat.getText().toString());

        Call<ResponseValue> call = APIConfig.getRetrofit().
                updateEmployee(preferences.getString("token",""),employee);
        call.enqueue(new Callback<ResponseValue>() {
            @Override
            public void onResponse(Call<ResponseValue> call, Response<ResponseValue> response) {
                progressDialog.dismiss();
                if (response.body() != null){
                    boolean status = response.body().isStatus();
                    String remarks = response.body().getRemarks();
                    if (status){
                        Toast.makeText(InputDataActivity.this,
                                remarks, Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else {
                        Toast.makeText(InputDataActivity.this,
                                remarks, Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(InputDataActivity.this,
                            "Save Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseValue> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(InputDataActivity.this,
                        "Save Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }


    void btnSimpanClick() {
        try {
            Employee book = new Employee(
                    txtNrp.getText().toString(),
                    txtNama.getText().toString(),
                    txtAlamat.getText().toString());
            book.save();

//            Employee employee = new Employee();
//            employee.setNrp(txtNrp.getText().toString());
//            employee.setNama(txtNama.getText().toString());
//            employee.save();

            clearData();
            finish();
//            getList();
        }catch (Exception ex){
            Log.d("guwe error",ex.toString());
        }
    }

    void btnEditClick() {
        try {
//            Employee book = Employee.find(Employee.class,
//                    "NRP = ?",
//                    new String[]{txtNrp.getText().toString()}).get(0);

            Employee employee = Select.from(Employee.class).where("NRP = ?",
                    new String[]{txtNrp.getText().toString()}).first();

            if (employee != null){
                employee.setNama(txtNama.getText().toString());
                employee.setAlamat(txtAlamat.getText().toString());
                employee.save();
                clearData();
                finish();
            }
            else {
                Toast.makeText(this, "Data Kosong", Toast.LENGTH_SHORT).show();
            }


//            getList();
        }catch (Exception ex){
            Log.d("guwe error",ex.toString());
        }
    }

    void getList(){
        List<Employee> Employees = Employee.listAll(Employee.class);
        Toast.makeText(getApplication(),String.valueOf(Employees.size()),
                Toast.LENGTH_LONG).show();
    }
    void clearData(){
        txtNrp.getText().clear();
        txtNama.getText().clear();
        txtAlamat.getText().clear();
    }
}
