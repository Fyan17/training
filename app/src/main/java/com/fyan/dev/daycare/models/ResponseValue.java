package com.fyan.dev.daycare.models;

public class ResponseValue {
    boolean status;
    String remarks;

    public boolean isStatus() {
        return status;
    }

    public String getRemarks() {
        return remarks;
    }
}
