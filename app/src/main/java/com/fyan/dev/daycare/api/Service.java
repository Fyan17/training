package com.fyan.dev.daycare.api;

import com.fyan.dev.daycare.models.Employee;
import com.fyan.dev.daycare.models.ResponseValue;
import com.fyan.dev.daycare.models.Token;
import com.fyan.dev.daycare.models.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Service {

    @POST("token")
    Call<Token> login(@Body User user);

    @GET("Training/getListEmployee")
    Call<List<Employee>> getListEmployee(@Header("Authorization") String token);

    @POST("Training/insertEmployee")
    Call<ResponseValue> insertEmployee(@Header("Authorization") String token,@Body Employee param);

    @POST("Training/updateEmployee")
    Call<ResponseValue> updateEmployee(@Header("Authorization") String token,@Body Employee param);

    @POST("Training/deleteEmployee")
    Call<ResponseValue> deleteEmployee(@Header("Authorization") String token,@Query("nrp") String nrp);
}
