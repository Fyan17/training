package com.fyan.dev.daycare.models;

public class Token {
    String token;
    String expired_date;
    int expired;
    boolean success;

    public String getToken() {
        return token;
    }

    public String getExpired_date() {
        return expired_date;
    }

    public int getExpired() {
        return expired;
    }

    public boolean isSuccess() {
        return success;
    }
}
