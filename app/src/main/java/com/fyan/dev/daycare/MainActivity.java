package com.fyan.dev.daycare;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import com.fyan.dev.daycare.adapter.AdapterEmployee;
import com.fyan.dev.daycare.api.APIConfig;
import com.fyan.dev.daycare.models.Employee;
import com.fyan.dev.daycare.models.ResponseValue;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    Button btnInput;
    RecyclerView rv_employee;
    AdapterEmployee adapterEmployee;
    LinearLayoutManager layoutManager;
    List<Employee> litEmployee;
    SwipeRefreshLayout swipeRefreshLayout;
    ProgressDialog progressDialog;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
//    GridLayoutManager gridLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Cek session login
        preferences = getApplicationContext().
                getSharedPreferences("training",MODE_PRIVATE);
        boolean isLogin = preferences.getBoolean("isLogin",false);
        if (!isLogin){
            Intent intent = new Intent(this, Login.class);
            startActivity(intent);
            finish();
        }

        btnInput =(Button) findViewById(R.id.btn_inpuform);
        rv_employee = (RecyclerView) findViewById(R.id.rv_employee);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);

//        layoutManager = new LinearLayoutManager(this,
//                LinearLayoutManager.HORIZONTAL,true);
//        gridLayoutManager = new GridLayoutManager(this,3);
        litEmployee = Employee.listAll(Employee.class);
        //List<Employee> litEmployee = getListEmployee();
        layoutManager = new LinearLayoutManager(this);
        rv_employee.setLayoutManager(layoutManager);
        adapterEmployee = new AdapterEmployee(this,litEmployee);
        rv_employee.setAdapter(adapterEmployee);

        btnInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),InputDataActivity.class);
                startActivity(intent);
            }
        });

        swipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_red_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_green_dark,
                android.R.color.holo_blue_dark);
        swipeRefreshLayout.setDistanceToTriggerSync(500);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadFromAPI();
            }
        });
    }

//    List<Employee> getListEmployee(){
//        List<Employee> iList = new ArrayList<>();
//        Employee emp = new Employee();
//        emp.setNrp("6212090");
//        emp.setNama("Supardi");
//        emp.setAlamat("Jakarta");
//        iList.add(emp);
//        return  iList;
//    }



    @Override
    protected void onStart() {
        super.onStart();
//        loadFromLocal();
        loadFromAPI();
    }

    void loadFromLocal(){
        litEmployee.clear();
        List<Employee> list = Employee.listAll(Employee.class);
        litEmployee.addAll(list);
        adapterEmployee.notifyDataSetChanged();
    }

    private void loadFromAPI(){
        swipeRefreshLayout.setRefreshing(true);
        Call<List<Employee>> call = APIConfig.getRetrofit().
                getListEmployee(preferences.getString("token",""));
        call.enqueue(new Callback<List<Employee>>() {
            @Override
            public void onResponse(Call<List<Employee>> call, Response<List<Employee>> response) {
                swipeRefreshLayout.setRefreshing(false);
                if (response.body() != null) {
                    litEmployee.clear();
                    List<Employee> list = response.body();
                    litEmployee.addAll(list);
                    adapterEmployee.notifyDataSetChanged();

                    //Insert all to local
                    Employee.deleteAll(Employee.class);
                    Employee.saveInTx(list);
                }
                else {
                    loadFromLocal();
                }
            }

            @Override
            public void onFailure(Call<List<Employee>> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                loadFromLocal();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public  void ConfirmDelete(final String nrp){
        new AlertDialog.Builder(this)
                .setTitle("Konfirmasi")
                .setMessage("Apakah anda yakin ?")
                .setIcon(R.drawable.ic_warning)
                .setCancelable(false)
                .setPositiveButton("YA", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                        deleteFromLocal(nrp);
                        deleteFromAPI(nrp);
                    }
                })
                .setNeutralButton("TIDAK",null)
                .show();
    }

    private void deleteFromLocal(String nrp){
        Employee employee = Select.from(Employee.class).
            where("NRP = ?",
            new String[]{nrp}).first();
        employee.delete();
        loadFromLocal();
    }

    private void deleteFromAPI(String nrp){
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Save to server");
        progressDialog.setCancelable(false);
        progressDialog.show();


        Call<ResponseValue> call = APIConfig.getRetrofit().
                deleteEmployee(preferences.getString("token",""),nrp);
        call.enqueue(new Callback<ResponseValue>() {
            @Override
            public void onResponse(Call<ResponseValue> call, Response<ResponseValue> response) {
                progressDialog.dismiss();
                if (response.body() != null){
                    boolean status = response.body().isStatus();
                    String remarks = response.body().getRemarks();
                    if (status){
                        Toast.makeText(MainActivity.this,
                                remarks, Toast.LENGTH_SHORT).show();
                        loadFromAPI();
                    }
                    else {
                        Toast.makeText(MainActivity.this,
                                remarks, Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(MainActivity.this,
                            "Save Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseValue> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(MainActivity.this,
                        "Save Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
