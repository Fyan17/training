package com.fyan.dev.daycare.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fyan.dev.daycare.InputDataActivity;
import com.fyan.dev.daycare.MainActivity;
import com.fyan.dev.daycare.R;
import com.fyan.dev.daycare.models.Employee;

import java.util.List;

public class AdapterEmployee extends RecyclerView.Adapter<AdapterEmployee.ViewHolder> {
    Context context;
    List<Employee> listEmployee;

    public AdapterEmployee(Context context, List<Employee> listEmployee) {
        this.context = context;
        this.listEmployee = listEmployee;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_karyawan,
                viewGroup,false);
        return new AdapterEmployee.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final Employee employee = listEmployee.get(i);
        viewHolder.txtNrp.setText(employee.getNrp());
        viewHolder.txtNama.setText(employee.getNama());
        viewHolder.txtAlamat.setText(employee.getAlamat());

        viewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, InputDataActivity.class);
                intent.putExtra("modeEdit", true);
                intent.putExtra("nrp",employee.getNrp());
                intent.putExtra("nama",employee.getNama());
                intent.putExtra("alamat",employee.getAlamat());
                context.startActivity(intent);
            }
        });

        viewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)context).ConfirmDelete(employee.getNrp());
            }
        });
//        if(employee.getNrp().equals("787")){
//
//            viewHolder.txtAlamat.setVisibility(View.GONE);
//        }
    }

    @Override
    public int getItemCount() {
        return listEmployee.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtNrp, txtNama,txtAlamat;
        ImageView btnEdit,btnDelete;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtNrp = itemView.findViewById(R.id.txtNrp);
            txtNama = itemView.findViewById(R.id.txtNama);
            txtAlamat = itemView.findViewById(R.id.txtAlamat);
            btnEdit = itemView.findViewById(R.id.btnEdit);
            btnDelete = itemView.findViewById(R.id.btnDelete);
        }
    }
}
